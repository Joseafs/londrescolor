<div class="container">
    <div class="content pdg30B">
        <h1 class="title cPrimary fDosis">Cat�logos</h1>
        
        <?php
            $dados=$conn->query("SELECT * FROM DOWNLOADCATEGORIA ORDER BY DS_DOWNLOADCATEGORIA ASC");

            if(count($dados) > 0){
                echo    "<div class='acd dInlineB w100 mgn20T'>";

                for($i =0; $i < count($dados);$i++){

                    $dadosArquivo=$conn->query("SELECT * FROM DOWNLOAD WHERE ID_DOWNLOADCATEGORIA=".$dados[$i]['ID_DOWNLOADCATEGORIA']." AND (NM_ARQUIVO != '' OR LINKEXTERNO != '')"); 

                    /* L�gica que seria usada : Exibir� todos que tenham informa��es relevantes, mesmo sem links de download ou externo E se houver links, mostrar campos respectivos */
                    if(count($dadosArquivo) > 0){
                        echo    "<div class='acd-sct'>",
                                    "<a class='acd-sct-title close-right bgPrimary cWhite fDosis fSize16 fBold'  href='#acd-".$i."'>"
                                        .$dados[$i]['DS_DOWNLOADCATEGORIA'].
                                    "</a>",
                                    "<div id='acd-".$i."' class='acd-sct-content'>";
                                        for($j =0; $j < count($dadosArquivo);$j++){
                                            $linkDownload = "desativo" ;
                                            $linkUrl = "desativo";

                                            echo "<div class='floatL w100 pdg3'>";
                                                if($dadosArquivo[$j]['LINKEXTERNO'] != '' && ($dadosArquivo[$j]['NM_ARQUIVO'] != '' && file_exists("./arquivos/download/".$dadosArquivo[$i]['NM_ARQUIVO'])) ){
                                                    echo    "<div class='powerLink linkDownload linkUrl floatL bgOpac-dark1 w100 pdg8 bRad3 ovflwH bShwOut-1p dMiddle' title='$dadosArquivo[$j]['NM_DOWNLOAD']' >";
                                                    $linkUrl = 'ativo';
                                                    $linkDownload = 'ativo';

                                                }else if($dadosArquivo[$j]['LINKEXTERNO'] != '' || ($dadosArquivo[$j]['NM_ARQUIVO'] != '' && file_exists("./arquivos/download/".$dadosArquivo[$i]['NM_ARQUIVO'])) ){
                                                    if($dadosArquivo[$j]['LINKEXTERNO'] != ''){
                                                        echo    "<div class='powerLink linkUrl floatL bgOpac-dark1 w100 pdg8 bRad3 ovflwH bShwOut-1p dMiddle' title='$dadosArquivo[$j]['NM_DOWNLOAD']' >";
                                                        $linkUrl = 'ativo';
                                                    }
                                                    if($dadosArquivo[$j]['NM_ARQUIVO'] != '' && file_exists("./arquivos/download/".$dadosArquivo[$i]['NM_ARQUIVO'])){
                                                        echo    "<div class='powerLink linkDownload floatL bgOpac-dark1 w100 pdg8 bRad3 ovflwH bShwOut-1p dMiddle mgn10B' title='$dadosArquivo[$j]['NM_DOWNLOAD']' >";
                                                        $linkDownload = 'ativo';
                                                    }
                                                }
                                                else{
                                                    echo    "<div class='powerLink floatL bgOpac-dark1 w100 bRad3 ovflwH bShwOut-1p dMiddle' title='$dadosArquivo[$j]['NM_DOWNLOAD']' >";
                                                }
                                                        echo    "<div class='floatL w100 cGray3 default'>",
                                                                    "<h4 class='cGray3 fSize16'>".$dadosArquivo[$j]['NM_DOWNLOAD']."</h4>",
                                                                    "<div class='fSize14 pdg5' >";
                                                                        if( $dadosArquivo[$j]['DS_DOWNLOAD'] != ''){
                                                                            echo $dadosArquivo[$j]['DS_DOWNLOAD'];
                                                                        } else {
                                                                            echo "Aguardando informa��es complementares"; 
                                                                        }
                                                            echo    "</div>",
                                                                "</div>";

                                                                if($linkUrl == 'ativo'){
                                                                    echo "<a class='fIcon pdg10 effRipple effShadow bgPrimary cWhite linkSpace dMiddle bRad3' title='".$dadosArquivo[$j]['LINKEXTERNO']."' href='".$dadosArquivo[$j]['LINKEXTERNO']."' target='_blank' rel='nofollow'>link</a>";
                                                                }
                                                                if ($linkDownload == 'ativo'){
                                                                    echo "<a class='fIcon pdg10 effRipple effShadow bgPrimary cWhite linkSpace dMiddle bRad3' title='".$dadosArquivo[$j]['NM_DOWNLOAD']."' href='/arquivos/download/".$dadosArquivo[$j]['NM_ARQUIVO']."' download >file_download</a>";
                                                                }
                                        echo                "</div>",
                                            "</div>";
                                        }
                        echo        "</div>",
                                "</div>";
                    }
                }
                echo "</div>";
            }
        ?>
    </div>
</div>