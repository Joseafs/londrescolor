<header class="w100 floatL pRelative zInd2 bgPrimary">
    <div class="content tCenter pdg5 pRelative">
        <a id="header-logo" class="dInlineB effScale bRad3 pdg5 max-w100" href="http://<?=$_SERVER['HTTP_HOST']; ?>" >
            <img class='floatL w100' src='/img/topoLogo.svg' alt='' title='' />
        </a>
        <div id="header-toggle" class="effRippleDark effToggle effShadow bRad3 ovflwH  floatR mgn10R">
            <div class="has-toggle"></div>
            <div class="has-toggle"></div>
            <div class="has-toggle"></div>
        </div>
    </div>
    <div id="header-menu" class="floatL w100 tCenter fBold">
        <div class="container">
            <div class='content'>
                <ul class="contMenu fSize20 tUppercase dInlineB fDosis">
                    <li><a class="effRippleDark" href="/a-empresa">A Empresa</a></li>
                    <li><a class="effRippleDark" href="/noticias">Notícias</a></li>
                    <li><a class="effRippleDark" href="/produtos">Produtos</a></li>
                    <li><a class="effRippleDark" href="/catalogos">Catálogos</a></li>
                    <li><a class="effRippleDark" href="/onde-comprar">Onde Comprar</a></li>
                    <li><a class="effRippleDark" href="/contato">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
