<div class="container">
    <div class="content pdg30B">
        <h1 class="title cPrimary fDosis">Produtos</h1>
        <div id='aside-left' class='fSize16 floatL pdg3T zInd999'>
            <h2 class='title cPrimary displayOff md-displayOn'>Categorias</h2>
            <div class='asideContent'>
                <?php
                
                    if($_GET['idCat']!="" && is_numeric($_GET['idCat'])){
                        $cat = $_GET['idCat'];
                    }
                    
                    $sql="SELECT DISTINCT MC.* FROM MATERIALCATEGORIA MC, MATERIAL M ".
								"WHERE M.ID_MATERIALCATEGORIA = MC.ID_MATERIALCATEGORIA ". 
								"AND M.ID_MATERIALPAGINA = 1 ORDER BY MC.DS_MATERIALCATEGORIA";
                    $dadosProdCategoria= $conn ->query($sql);
                    
                    if(count($dadosProdCategoria)> 0 ){
                         for($i = 0; $i < count($dadosProdCategoria); $i++){
                             
                            if($cat == $dadosProdCategoria[$i]['ID_MATERIALCATEGORIA']){
                                echo "<a class='bgWhite mgn3B floatL w100 effShadow effRippleDark pdg8 cPrimary bRad3 catProdAtivo' href='/categoria/".$dadosProdCategoria[$i]['ID_MATERIALCATEGORIA']."/".Link::getStringBarra($dadosProdCategoria[$i]['DS_MATERIALCATEGORIA'])."' title='".$dadosProdCategoria[$i]['DS_MATERIALCATEGORIA']."' >".$dadosProdCategoria[$i]['DS_MATERIALCATEGORIA']."</a>";

                            } else{
                        
                                echo "<a class='bgPrimary mgn3B floatL w100 effShadow effRippleDark pdg8 cWhite bRad3' href='/categoria/".$dadosProdCategoria[$i]['ID_MATERIALCATEGORIA']."/".Link::getStringBarra($dadosProdCategoria[$i]['DS_MATERIALCATEGORIA'])."' title='".$dadosProdCategoria[$i]['DS_MATERIALCATEGORIA']."' >".$dadosProdCategoria[$i]['DS_MATERIALCATEGORIA']."</a>";
                            }
                        }
                    }
                ?>
            </div>
            <div id='aside-leftBtn' class="effRippleDark effToggle toggle-arrow effShadow bRad3 ovflwH bgPrimary displayOff md-displayOn">
                <div class="has-toggle"></div>
                <div class="has-toggle"></div>
                <div class="has-toggle"></div>
            </div>
        </div>
        
        <div id='pg-view-aside' class='floatR md-w100'>
            <div class='w100 floatL contBox tCenter'>
                <?php
                    
                    $totalPorPag = 12;
                    $paginacao = new Paginacao($conn, "SELECT M.* FROM MATERIAL M WHERE M.ID_MATERIALPAGINA=1 ORDER BY M.NM_MATERIAL ASC", $totalPorPag, $_POST);
                    
                    if($_GET['idCat']!="" and is_numeric($_GET['idCat'])){

                        $sqlProdutos="SELECT M.* FROM MATERIAL M WHERE M.ID_MATERIALPAGINA=1 AND ID_MATERIALCATEGORIA=".$cat." ORDER BY M.NM_MATERIAL ASC LIMIT ".$paginacao->getInicio().",".$paginacao->getFinal();
                    } else {
                        $sqlProdutos="SELECT M.* FROM MATERIAL M WHERE M.ID_MATERIALPAGINA=1 ORDER BY M.NM_MATERIAL ASC LIMIT ".$paginacao->getInicio().",".$paginacao->getFinal();
                    }
                    
                    $dados = $conn->query($sqlProdutos);

                    if(count($dados) > 0){

                        for($i = 0; $i < count($dados); $i++){
                            echo    "<div class='box4 pdg3 pdg10B'>".
                                        "<a class='contLimit effRipple dInlineB effZoom pdg5' href='".Link::getLink("produto", array($dados[$i]['ID_MATERIAL'], Link::getStringBarra($dados[$i]['NM_MATERIAL'])))."' title='".$dados[$i]['NM_MATERIAL']."' >";

                                            if (file_exists("./arquivos/material/".$dados[$i]['ID_MATERIAL'].".png")) {
                                                echo "<img class='floatL puny-child spy-child' src='/arquivos/material/".$dados[$i]['ID_MATERIAL'].".png' alt='".$dados[$i]['NM_MATERIAL']."' />";
                                            } else {
                                                echo "<img class='floatL puny-child spy-child' src='/img/semImgProduto.png' alt='".$dados[$i]['NM_MATERIAL']."' />";
                                            }
                                            echo    "<h3 class='floatL w100 pdg5 cGray3 fSize16 fDosis'>"
                                                        .(strlen($dados[$i]['NM_MATERIAL']) > 25 ? substr($dados[$i]['NM_MATERIAL'], 0, 25) . "..." : $dados[$i]['NM_MATERIAL']).
                                                    "</h3>";
                                            
                                echo    "</a>".
                                    "</div>";
                        }
                        echo    "<div class='w100 floatL pdg20 fSize16'>";
                                    $paginacao->show();
                        echo    "</div>";
                        
                    } else {
                        echo "<h3 class='title cGray3 mgn30T'>Sem itens no estoque !</h3>";
                        //header("Location: http://".$_SERVER['HTTP_HOST']);
                    }
                ?>
            </div>
        </div>
    </div>
</div>
