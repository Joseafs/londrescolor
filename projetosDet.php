<script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '212306842484368',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
</script>

<div class="container">
    <div class="content pdg30B sm-tCenter">
        <?php
            $idSecao = $_GET['id'];
            if(is_numeric($idSecao) && $idSecao > 0){
                $sql = "SELECT * FROM GALERIA WHERE ID_GALERIA=".$idSecao;
                $dadosSecao = $conn->query($sql);
                
                if(!empty($dadosSecao)){

                    echo '<h1 class="title cPrimary fDosis">'.$dadosSecao[0]['NM_GALERIA'].'</h1>';
                    
                    if(file_exists("./arquivos/fotodestaque/".$dadosSecao[0]['ID_GALERIA'].".jpg")) {
                        $imgSecao = "<img class='floatL bRad3 sampleImg' src='/arquivos/fotodestaque/".$dadosSecao[0]['ID_GALERIA'].".jpg' alt='".$dadosSecao[0]['DS_TITULO']."' title='".$dadosSecao[0]['DS_TITULO']."' />";
                        $imgSecaoFace = "http://".$_SERVER['HTTP_HOST']."/arquivos/fotodestaque/".$dadosSecao[0]['ID_GALERIA'].".jpg";
                    }
        ?>
                    <div class="w100 floatL mgn20B tCenter">
                        <div class="socialIcones dInlineB tLeft sm-tCenter">
                            <div class="floatL sm-clearB sm-w100">
                                <a href="http://twitter.com/share" class="twitter-share-button" data-text="" data-count="horizontal">Tweet</a>
                                <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                                <script src="http://platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share" data-counter="right"></script>&nbsp;&nbsp;
                                <g:plusone size="medium"></g:plusone>
                            </div>
                            <div class="floatL sm-clearB sm-w100">
                                <button id="share_button" class="bRad3 effRipple floatL" alt="Compartilhar" title="Compartilhar" type="submit">Compartilhar</button>
                                <div class="socialIconesFacebook sm-floatR floatL mgn5L">
                                    <div class="fb-like" data-width="20" data-href="<?=$config->getConfig(7)?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                            $('#share_button').click(function(e){
                                                e.preventDefault();
                                                FB.ui(
                                                {
                                                    method: 'feed',
                                                    name: '<?=$dadosSecao[0]['NM_GALERIA'] ?>',
                                                    link: '<?=$_SERVER['SCRIPT_URI']; ?>',
                                                    picture: '<?=$imgSecaoFace;?>',
                                                    caption: '<?=$dadosSecao[0]['DS_SUBTITULO'] ?>',
                                                    description: '',
                                                    message: ''
                                                });
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="default floatL w100 tCenter">
                        <?php echo $imgSecao;?>
                        <div class="fSize16 tLeft">
                            <div class="fSize16 cGray6 tCenter pdg8 fItalic"><?=strtolower(strftime("%d/%b/%Y", strtotime($dadosSecao[0]['DT_GALERIA']))) ?></div>
                            <?=stripslashes($dadosSecao[0]['DS_GALERIA']) ?>
                        </div>
                        
                    </div>
                    <div class='contBox floatL w100 fSize16 pdg10B'>
                        <?php
                            if (count($dadosSecao) > 0) {

                                echo '<div id="galeria" class="w100 floatL contBox tCenter pdg40T">';

                                    $fotos = $conn->query("SELECT * FROM GALERIAFOTO WHERE ID_GALERIA=".$dadosSecao[0]['ID_GALERIA']);
                                    if(count($fotos)>0){
                                        for ($i = 0; $i < count($fotos); $i++) {
                                            echo "<div class='box5 pdg3 pdg15B md-w33 sm-w50'>",
                                                '<a class="contLimit dInlineB effRipple effShadow ovflwH bRad3 view-iframe" style=" background: url(/foto/'.str_replace(" ", "%20", $dadosSecao[0]['PASTA']).'/'.str_replace(" ", "%20", $fotos[$i]['NM_ARQUIVO']).') center center no-repeat; background-size:200%;" rel="'.$dadosSecao[0]['NM_GALERIA'].'" href="/foto/'.str_replace(" ", "%20", $dadosSecao[0]['PASTA']).'/'.str_replace(" ", "%20", $fotos[$i]['NM_ARQUIVO']).'"  title="'.$fotos[$i]['DS_GALERIAFOTO'].'"></a>',
                                            "</div>";
                                        }
                                    }else{
                                            echo "<h3 class='title cGray3 mgn30T'>Nenhuma foto cadastrada.</h3>";
                                    }
                                echo '</div>';
                            }
                        ?> 
                    </div>
        <?php
                } else { header("Location: http://".$_SERVER['HTTP_HOST']);}
            } 
        ?>
        <a class="floatR bRad3 cPrimary fSize20 effRippleDark effShadow pdg8 ovflwH sm-w100" href="/projetos" alt="mais projetos" title="mais projetos" >+ projetos</a>
    </div>
</div>