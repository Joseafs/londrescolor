<?php
    include_once("_cabecalho.php");
?>
<html>
     <head>
        <!-- 12/02/2016 -->
        <title> Londres Color </title>
        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <meta name="keywords" content="londrescolor, Londres Color, tintas, cores, realeza, your house, sua casa, colorido, comprar tinta, cores da realeza, tinta leao" />
        <meta name="Description" content="Londres Color" />
        <meta name="author" content="USES SOFTWARE E DESIGN - www.uses.com.br" />
        <meta name="robots" content="noindex, nofollow" />
        
        <meta property="og:title" content="Londres Color" />
        <meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST']; ?>" />
        <meta property="og:site_name" content="Londres Color" />
        <meta property="og:image" content="http://<?=$_SERVER['HTTP_HOST']; ?>/img/topoLogo.png" />
        <meta property="og:description" content="Londres Color" />
        <meta name="viewport" content="width=device-width, initial-scale=1 , minimum-scale=0.5 ,maximum-scale=2"> 

        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#ED1C24">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#ED1C24">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#ED1C24">
        
        <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="/img/favicon/manifest.json">
        
        <link type="text/css" rel="stylesheet" href="/css/londrescolor.css?<?=time();?>" >
        <link type="text/css" rel="stylesheet" href="/css/londrescolorResponsivo.css?<?=time();?>" > 
        <link type="text/css" rel="stylesheet" href="/css/rs-minimal-white.css" >
        <link type="text/css" rel="stylesheet" href="/css/jcarousel.responsive.css" >
        <link type="text/css" rel="stylesheet" href="/css/royalslider.css" >
        <link type="text/css" rel="stylesheet" href="/css/jquery.materialripple.css" >
        <link type="text/css" rel="stylesheet" href="/source/jquery.fancybox.css" >
        <link type="text/css" rel="stylesheet" href="/source/helpers/jquery.fancybox-thumbs.css" >
        <link type="text/css" rel="stylesheet" href="/css/efeitos.css?<?=time();?>">
        <link type="text/css" rel="stylesheet" href="/css/slick.css?<?=time();?>">
        
        <link type="text/css" rel="stylesheet" href="/css/mobile.css?<?=time();?>" media="all and (max-width: 768px)" title="Nexus Low">
        <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
        
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
            {lang: 'pt-BR'}
        </script>
        <script type="text/javascript" src="/js/maps.js"></script>
    </head>
    <body>
        <?php include_once 'topo.php'; ?>
        <section class="zInd1 pRelative floatL w100">
            
            <?php 
                if(!file_exists($_GET["cod"].".php") || $_GET["cod"]=="home"){
                    echo "<div  id='home-slider' class='royalSlider rsMinW pRelative pdg40B'>";

                            $sql="SELECT B.* FROM BANNER B WHERE B.ID_BANNERPOSICAO=1 AND B.TP_STATUS='A' AND CURRENT_DATE BETWEEN B.DT_INICIO AND B.DT_FIM ORDER BY RAND() LIMIT 0,4";
                            $bannerPrincipal=$conn->query($sql);
                            if(count($bannerPrincipal)>0){
                                
                                for($i=0;$i < count($bannerPrincipal); $i++){
                                    echo '<a href="'.(empty($bannerPrincipal[$i]['LINK'])?"javascript:void(0);":(substr($bannerPrincipal[$i]['LINK'],0,4)!="http"?"http://".$bannerPrincipal[$i]['LINK']:$bannerPrincipal[$i]['LINK'])).'" '.(!empty($bannerPrincipal[$i]['LINK'])?"target='".$bannerPrincipal[$i]['TARGET']."'":"").' id="banner_'.$bannerPrincipal[$i]['ID_BANNER'].'" class="linkbannerdestaque clickbannerstats">';
	                                    echo    "<div class='rsContent' title='".$bannerPrincipal[$i]['DS_BANNER']."'>",
	                                                "<img class='rsImg' alt='".$bannerPrincipal[$i]['DS_BANNER']."' src='/arquivos/banners/".$bannerPrincipal[$i]['NM_IMAGEM']."' />",
	                                            "</div>";
									echo '</a>';
                                }
                            } else {
                                echo    "<div class='rsContent'>",
                                            "<img class='rsImg' title='Sem Banner Cadastrado'  alt='Sem Banner Cadastrado' src='/img/semImgBanner.jpg' />",
                                        "</div>";
                            }
                    echo "</div>";
                } 
                if(!file_exists($_GET["cod"].".php") || $_GET["cod"]=="" || is_numeric($_GET["cod"]) || $_GET["cod"]=="home"){
                    
                    include_once ("home.php");
                } else {
                    
                    echo "<div id='page-wrap' class='w100 floatL'>";
                    include_once ($_GET["cod"].".php");
                    echo "</div>";
                }
            ?>
        </section>
        <?php include_once 'rodape.php'; ?>
            
        <script type="text/javascript" src="/js/jquery.maskedinput-1.3.1.min.js"></script>
        <script type="text/javascript" src="/source/jquery.fancybox.js"></script>
        <script type="text/javascript" src="/source/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script type="text/javascript" src="/js/jquery.royalslider.min.js"></script>
        <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="/js/jcarousel.responsive.js"></script>
        <script type="text/javascript" src="/js/jquery.materialripple.js"></script>
        <script type="text/javascript" src="/js/slick.js"></script>
        
        <script type="text/javascript" src="/js/script.js?<?=time();?>"></script>
    </body>
</html>
<?php
    $conn->desconectar();
?>
