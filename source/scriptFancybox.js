$(function(){
/* -------------------------------------------------------------------------- */
// fancybox //  
    $(".fancybox").fancybox({
        openEffect: 'fade',
        titleShow: true,
        openMethod:'changeIn',
        closeBtn: false,
        padding: 2,
        helpers: {
            overlay :  {
                locked : false // item n�o vai ao TOP quando clicado
            },
            title: {
                type: 'float',
                title   : true
            },
            thumbs: {
                width: 50,
                height: 50,
                position: 'top'
            }
        }
    });
    
    $(".fancyboxIframe").fancybox({
        //maxWidth	: 1260,
        //maxHeight	: 800,
        fitToView	: false,
        width       : '95%',
        height      : '95%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'fade',
        closeEffect	: 'fade',
                
        iframe: {
            scrolling : 'auto',
            preload   : true
        },
        helpers: {
            overlay :  {
                locked : false // item n�o vai ao TOP quando clicado
            }
        }
    });
});