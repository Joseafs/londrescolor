<?php
if ($_POST['btnEnviar']=="Enviar") {

    $erro = "";

    if ($nome->isEmpty()) {
        $erro = "Preencha seu nome";
    } elseif ($email->isEmpty() || !$email->isMail()) {
        $erro = "Informe seu e-mail";
    } elseif ($telefone->isEmpty()) {
        $erro = "Informe o telefone";
    } elseif ($cidade->isEmpty()) {
        $erro = "Informe o nome da cidade";
    } elseif ($estado->isEmpty()) {
        $erro = "Informe o estado";
    }

    if (empty($erro)) {
        $emailConteudo = $conn->query("SELECT * FROM CONTEUDOEMAIL WHERE ID_CONTEUDOEMAIL=1");
        $emailConteudo[0]['CONTEUDO'] = str_replace("%NOME%", $_POST['nome'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%EMAIL%", $_POST['email'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%TELEFONE%", $_POST['telefone'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%CIDADE%", $_POST['cidade'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%ESTADO%", $_POST['estado'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%ASSUNTO%", $_POST['assunto'], $emailConteudo[0]['CONTEUDO']);
        $emailConteudo[0]['CONTEUDO'] = str_replace("%MENSAGEM%", nl2br($_POST['mensagem']), $emailConteudo[0]['CONTEUDO']);

        $enviaremail = new Mail("LONDRES COLOR - CONTATO VIA WEBSITE", $config->getConfig(7), $emailConteudo[0]['ASSUNTO'], $emailConteudo[0]['CONTEUDO']);
        $enviaremail->setReplyTo($_POST['nome'], $_POST['email']);
        $enviaremail->setTo("LONDRES COLOR - CONTATO VIA WEBSITE", $config->getConfig(7));
        if ($enviaremail->enviar()) {
            $conn->beginTransaction();
            $sql = "INSERT INTO LOGCONTATO(ID_LOGCONTATO, NM_LOGCONTATO, EMAIL, TELEFONE, CIDADE, ESTADO, ASSUNTO, MENSAGEM) ".
                    "VALUES('',".$nome->getValorSql().",".$email->getValorSql().",".$telefone->getValorSql().",".$cidade->getValorSql().",".$estado->getValorSql().",".$assunto->getValorSql().",".$mensagem->getValorSql().") ";
            $conn->query($sql);
            $conn->endTransaction();
            
            $_POST = array();
            echo "<script>alert('Sua mensagem foi enviada com sucesso.\\n\\nEntraremos em contato o mais breve possivel.\\n\\nObrigado!')</script>";
        }else{
            echo "<script>alert('Erro no envio do contato.\\n\\nTente novamente!')</script>";
        }
    } else {
        echo "<script>alert('".$erro."')</script>";
    }
}
?>


<div class="floatL w100 zInd2 pRelative bShadowBottom" style="margin-bottom: 400px;">
    <div class="container">
        <div class="content pdg10B">
            <h1 class="title cPrimary fDosis">Contato</h1>
            <form class="formInfo floatL w50 pdg10 pdg30T sm-w100" method="POST" action="" >
                <div class="inputField w100 pdg3">
                    <input id="iptNome" type="text" placeholder="Nome" name="nome" class="selectField bgOpac20-dark" required>
                </div>
                <div class="inputField w60 pdg3">
                    <input id="iptEmail" type="email" placeholder="E-mail" name="email" class="selectField bgOpac20-dark" required>
                </div>
                <div class="inputField w40 pdg3">
                    <input id="iptFone" type="tel" placeholder="Telefone" name="telefone" class="selectField bgOpac20-dark fone" required>
                </div>
                <div class="inputField w60 pdg3">
                    <input id="iptCidade" type="text" placeholder="Cidade" name="cidade" class="selectField bgOpac20-dark" required>
                </div>
                <div class="inputField w40 pdg3">
                     <select id="sltEstado" name="estado" class="selectField bgOpac20-dark cGray3" required>
                        <option value="" class="displayOff">Estado</option>
                        <?php
                            $estados = $conn->query("SELECT * FROM ESTADO ORDER BY DS_ESTADO");
                            for ($i = 0; $i < count($estados); $i++) {
                                echo "<option value='".$estados[$i]['ID_ESTADO']."'>".$estados[$i]['DS_ESTADO']."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="inputField w100 pdg3">
                    <input id="iptAssunto" type="text" placeholder="Assunto" name="assunto" class="selectField bgOpac20-dark" required>
                </div>
                <div class="inputField w100 pdg3 pRelative pdg60R" >
                    <textarea id="txtMessage" placeholder="Mensagem" name="mensagem" class="selectField bRad3L bgOpac20-dark"></textarea>
                    <button name="btnEnviar" value="Enviar" class="bgPrimary bRad3R cWhite effRipple floatR effShadow pntPointer pRelative" style="width: 57px;" type="submit" >
                        <i class="fIcon dInlineB pdg3 fSize20">send</i>
                        <span class="pdg5 w100 floatL clearB fSize14">Enviar</span>
                    </button>
                </div>
            </form>
            <div class="floatR w50 pdg20L tCenter sm-w100">
                <h3 class="title cPrimary">Utilize o formulário, ou se preferir:</h3>
                <div class="default floatL fSize16 w100 pdg8 tLeft">
                    <?php
                        $dados=$conn->query("SELECT * FROM INFORMACAO WHERE ID_INFORMACAOCATEGORIA=2");
                        echo $dados[0]['DS_INFORMACAO'];
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w100 floatL pAbsolute pBottom pLeft ovflwH min-hgt400p zInd-1" > 
    <div id="localMaps" class="clickOff pAbsolute pTop pLeft w100 zInd0" ></div>
    <script src="https://maps.googleapis.com/maps/api/js?signed_in=false&callback=initMap" async defer></script>
</div>