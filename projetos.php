<div class="container">
    <div class="content pdg30B">
        <h1 class="title cPrimary fDosis">Projetos Realizados</h1>
        <div class='w100 floatL contBox tCenter'>
            <?php
                $sql="SELECT * FROM GALERIA WHERE BO_PUBLICAR='S' ORDER BY ID_GALERIA DESC";
                    $dados = $conn->query($sql);

                    if(count($dados) > 0){

                        for($i = 0; $i < count($dados); $i++){
                            echo    "<div class='box4 pdg3 pdg10B'>".
                                        "<a class='contLimit effRipple effHouse dInlineB effZoom bRad3 ovflwH' href='".Link::getLink("projeto", array($dados[$i]['ID_GALERIA'], Link::getStringBarra($dados[$i]['NM_GALERIA'])))."' title='".$dados[$i]['NM_GALERIA']."' >";

                                            if (file_exists("./arquivos/fotodestaque/".$dados[$i]['ID_GALERIA'].".jpg")) {
                                                echo "<img class='floatL puny-child spy-child' src='/arquivos/fotodestaque/".$dados[$i]['ID_GALERIA'].".jpg' alt='".$dados[$i]['NM_GALERIA']."' />";
                                            } else {
                                                echo "<img class='floatL puny-child spy-child' src='/img/semImgGaleria.png' alt='".$dados[$i]['NM_GALERIA']."' />";
                                            }
                                            echo    "<div class='zInd1 blop-down cWhite'>".
                                                        "<h3 class='bgOpac40-dark pdg5 fSize16 fDosis'>"
                                                            .(strlen($dados[$i]['NM_GALERIA']) > 25 ? substr($dados[$i]['NM_GALERIA'], 0, 25) . "..." : $dados[$i]['NM_GALERIA']).
                                                        "</h3>".
                                                    "</div>".
                                        "</a>".
                                    "</div>";
                        }
                    } else {
                        header("Location: http://".$_SERVER['HTTP_HOST']);
                    }
                ?>
        </div>
    </div>
</div>