<footer class='bgPrimary pdg30T zInd1 pRelative floatL w100' >
    <div class="container">
        <div class="default cWhite floatL fThin sm-w100 w70 fDosis pdg5B fSize32 ovflwV">
        	<?=$config->getConfig(1)?>
            <br />
            <div class="dMiddle">
                <a class='fIcon fSize42 cWhite effRippleDark floatL mgn5R bRad50P pRelative ovflwV effHouse fancyboxIframe'  data-fancybox-type='iframe' href='/pg-mapa.php'>
                    person_pin_circle
                    <h3 class="hvrText fSize24 fDosis cptH-child cptV-child bgWhite cPrimary"><span class="pdg15">Tra�ar rota</span></h3>
                </a>
                <span class="fSize20"><?=$config->getConfig(2)?></span>
            </div>
        </div>
        <div class="floatR sm-w100 tCenter pdg20T pdg5">
            <?php 
                if($config->getConfig(3)!= ""){
                    echo    "<a class='fIcon fIcon-uses dInlineB fSize42 cWhite bRad3 ovflwH effRippleDark effScale mgn10R' target='_blank' rel='nofollow' href='".$config->getConfig(3)."'>&#xe001;</a>";
                }
                if($config->getConfig(4)!= ""){
                    echo    "<a class='fIcon fIcon-uses dInlineB fSize42 cWhite bRad3 ovflwH effRippleDark effScale mgn10R' target='_blank' rel='nofollow' href='".$config->getConfig(4)."'>&#xe006;</a>";
                }
                if($config->getConfig(5)!= ""){
                    echo    "<a class='fIcon fIcon-uses dInlineB fSize42 cWhite bRad3 ovflwH effRippleDark effScale mgn10R' target='_blank' rel='nofollow' href='".$config->getConfig(5)."'>&#xe002;</a>";
                }
                if($config->getConfig(6)!= ""){
                    echo    "<a class='fIcon fIcon-uses dInlineB fSize42 cWhite bRad3 ovflwH effRippleDark effScale ' target='_blank' rel='nofollow' href='mailto:".$config->getConfig(6)."'>&#xe000;</a>";
                }
            ?>
        </div>
    </div>
    <div class='w100 floatL bgOpac20-dark tCenter mgn30T ovflwH'>
        <a class='dInlineB effRipple effScale effOpac pdg20L pdg20R pdg10T pdg5B ovflwV bRad3' target="_blank" href='http://www.uses.com.br/' title='Uses Software & Design'>
            <img class='floatL puny-child' width="75" src='/img/rodape_uses.svg' alt='Uses Software & Design'/>
        </a>
    </div>
</footer>