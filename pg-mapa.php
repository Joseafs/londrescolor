<?php
    include_once("_cabecalho.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- 19/02/2016 -->
        <title> Rota Londres Color </title>
        <meta name="viewport" content="width=device-width, initial-scale=1 , minimum-scale=0.5 ,maximum-scale=2,  user-scalable=no">

        <link type="text/css" rel="stylesheet" href="/css/londrescolor.css?<?=time();?>" >
        <link type="text/css" rel="stylesheet" href="/css/londrescolorResponsivo.css?<?=time();?>" > 
        <link type="text/css" rel="stylesheet" href="/css/efeitos.css?<?=time();?>" />
        <link type="text/css" rel="stylesheet" href="/css/mobile.css?<?=time();?>" media="all and (max-width: 768px)" title="Nexus Low">
      
    </head>
    <body class="iframe-body">
        <div class='container'>
            <div class='content'>
                <h1 class='title cPrimary'>Venha nos visitar!</h1>
                <div class='w100 floatL tCenter pdg20B'>
                    <form class='formInfo dInlineB w100' method="post" action="">
                            <div class='inputField w100 pdg5B'>
                                <label class='iptTitle' for="txtEnderecoPartida">Endere�o de partida:</label>
                                <input class='selectField bgOpac20-dark' type="text" id="txtEnderecoPartida" name="txtEnderecoPartida" />
                            </div>
                            <div class='inputField w100 displayOff'>
                                <label for="txtEnderecoChegada">Endere�o de chegada:</label>
                                <input class='selectField bgOpac10-dark' type="text" id="txtEnderecoChegada" name="txtEnderecoChegada" value='-23.2889718,-51.0870786' />
                            </div>
                            <div class='inputField w100'>
                                <input class='bgPrimary bRad3 cWhite dInlineB effShadow effRippleDark pntPointer pdg10 pdg20L pdg20R fSize16 sm-w100' type="submit" name="btnEnviar" value="Tra�ar Rota" />
                            </div>
                    </form>
                </div>
                <div id="rotaMaps" class='min-hgt400p sm-w100 floatL bRad3 ovflwH'></div>
                <div id="rotaTxt" class='floatR sm-w100'></div>
            </div>
        </div>
        
        <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.1&sensor=false&language=pt-BR"></script>

        <script type="text/javascript" src="/js/rotaMaps.js"></script>
        
    </body>
</html>