function jcarouselFull(opt) {
        var jcarousel = $('.' + opt.parent);
        if(jcarousel.length <= 0) {
            return false;
        }
        
        jcarousel.on('jcarousel:resize jcarousel:reload jcarousel:load  jcarousel:create jcarousel:scroll', function () {
            var carousel = $(this),
                width = carousel.innerWidth();
                
            opt.responsive.forEach(function(item) { 
                if(typeof item.operation !== 'undefined') {
                    if(item.operation === '<' && width < item.window) { 
                        item.extra(opt);
                        if(typeof item.extra !== 'undefined') {
                            item.extra(opt);
                        }
                    }
                    if(item.operation === '>=' && width >= item.window) {
                        width = width / item.value;
                        if(typeof item.extra !== 'undefined') {
                            item.extra(opt);
                        }
                    }
                }
            });
            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        })
        .jcarousel({wrap: 'circular'});

        $('.' + opt.parent).jcarouselAutoscroll({
            interval: opt.interval,
            target: '+=1',
            autostart: true
        });
        
        $('.' + opt.prev).jcarouselControl({ target: '-=1'});
        $('.' + opt.next).jcarouselControl({ target: '+=1'});
    }
    
$(function() {
    jcarouselFull({
        'parent': 'jc-content3',
        'prev': 'jc-ctrl-prev3',
        'next': 'jc-ctrl-next3',
        'interval': 2000,
        'responsive': [
            { window: 660, value: 3, operation: '>=' },
            { window: 400, value: 2, operation: '>=' }
        ]
    });
    
    jcarouselFull({
        'parent': 'jc-content4',
        'prev': 'jc-ctrl-prev4',
        'next': 'jc-ctrl-next4',
        'interval': 8000,
        'responsive': [
            { window: 900, value: 4, operation: '>=' },
            { window: 600, value: 3, operation: '>=' },
            { window: 380, value: 2, operation: '>=' }
        ]
    });
    jcarouselFull({
        'parent': 'jc-content5',
        'prev': 'jc-ctrl-prev5',
        'next': 'jc-ctrl-next5',
        'interval': 8000,
        'responsive': [
            { window: 1023, value: 0, operation: '<', extra: function(opt) {
                $('.' + opt.prev + ', .' + opt.next).css('display', 'block');
                $('.' + opt.parent).addClass('controlsActived');
            } },
            { window: 1024, value: 5, operation: '>=', extra: function(opt) {
                $('.' + opt.prev + ', .' + opt.next).css('display', 'none');
                $('.' + opt.parent).removeClass('controlsActived');

            } },
            { window: 767, value: 4, operation: '>=' },
            { window: 514, value: 3, operation: '>=' },
            { window: 280, value: 2, operation: '>=' },
        ]
    });
    jcarouselFull({
        'parent': 'jc-content6',
        'prev': 'jc-ctrl-prev6',
        'next': 'jc-ctrl-next6',
        'interval': 5000,
        'responsive': [
            { window: 1023, value: 0, operation: '<', extra: function(opt) {
                $('.' + opt.prev + ', .' + opt.next).css('display', 'block');
                $('.' + opt.parent).addClass('controlsActived');
            } },
            { window: 1024, value: 6, operation: '>=', extra: function(opt) {
                $('.' + opt.prev + ', .' + opt.next).css('display', 'none');
                $('.' + opt.parent).removeClass('controlsActived');
            } },
            { window: 854, value: 5, operation: '>=' },
            { window: 684, value: 4, operation: '>=' },
            { window: 514, value: 3, operation: '>=' },
            { window: 280, value: 2, operation: '>=' }
        ]
    });
});
        