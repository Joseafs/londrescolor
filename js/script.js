$(function(){
/* -------------------------------------------------------------------------- */
/* Banner */
    $('#home-slider').royalSlider({
        arrowsNav: true,
        loop: true,
        keyboardNavEnabled: true,
        controlsInside: false,
        imageScaleMode: 'fill',
        arrowsNavAutoHide: false,
        autoScaleSlider: true,
        autoScaleSliderWidth: 1920,
        autoScaleSliderHeight: 800,
        controlNavigation: 'bullets', 
        thumbsFitInViewport: false,
        navigateByClick: true,
        startSlideId: 0,
        autoPlay: {
            enabled: true,
            pauseOnHover: false,
            stopAtAction: false,
            delay: 7000
        },
        /* autoPlay: true, */
        transitionType: 'move', /* 'fade' ou 'move' */
        easeInOut: true,
        globalCaption: false,
        deeplinking: {
            enabled: true,
            change: false
        },
        imgWidth: 1920,
        imgHeight: 800
    });
/* -------------------------------------------------------------------------- */
$('#page-slider').royalSlider({
        arrowsNav: true,
        loop: true,
        keyboardNavEnabled: true,
        controlsInside: false,
        imageScaleMode: 'fill',
        arrowsNavAutoHide: false,
        autoScaleSlider: true,
        autoScaleSliderWidth: 1920,
        autoScaleSliderHeight: 320,
        controlNavigation: 'bullets', 
        thumbsFitInViewport: false,
        navigateByClick: true,
        startSlideId: 0,
        autoPlay: {
            enabled: true,
            pauseOnHover: false,
            stopAtAction: false,
            delay: 7000
        },
        /* autoPlay: true, */
        transitionType: 'move', /* 'fade' ou 'move' */
        easeInOut: true,
        globalCaption: false,
        deeplinking: {
            enabled: true,
            change: false
        },
        imgWidth: 1920,
        imgHeight: 320
    });
    
    
/* -------------------------------------------------------------------------- */
    $("#btnScrollTop").click(function() {
        $('html, body').animate({scrollTop: 0}, 1500);
    });
    
    $("#aside-left #aside-leftBtn").click(function(){
        if($("#aside-left").css("left") < "1"){
            $("#aside-left").animate({"left": "+1px"}, "slow");
            $("#aside-left #aside-leftBtn").addClass('active');
        }else{
            $("#aside-left").animate({"left": "-240px",}, "slow");
            $("#aside-left #aside-leftBtn").removeClass('active');
        }
    });
    
     $("#header-toggle").click(function(){
        if($("#aside-right").css("right") < "1"){
            $("#aside-right").animate({"right": "+1px", "margin-right": "-1px"}, "slow");
            $("header .aside-rightBtn").removeClass('open').addClass('close'); /* N�o recebe active */
            $("#aside-right .aside-rightBtn, #aside-right .header-logo").addClass('open active').removeClass('close');
        }else{
            $("#aside-right").animate({"right": "-100%",}, "slow");
            $("#aside-right .aside-rightBtn").removeClass('open active').addClass('close'); /* Recebe active para animacao */
            $("header .aside-rightBtn, header .header-logo").addClass('open').removeClass('close');
        }
    });
    
    $("#header-toggle").click(function(){
            $("#header-toggle").toggleClass('active');
            $("#header-menu").slideToggle();
    });
        
    $('.effRipple, .effRippleDark, .contMenu a.effActv, .subMenu a').materialripple();
     
    $('.btnActv').click(function(e) {
        e.preventDefault();
        $(this).addClass('active');
    });
            
/* -------------------------------------------------------------------------- */
// Form //  
    $("input.cep").mask("99999-999");
    $("input.n_casa").mask("9999999");
    $("input.cpf").mask("999.999.999.99");
    $("input.rg").mask("99.999.999.9");
    $("input.fone").focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
   
/* -------------------------------------------------------------------------- */
// Blinds //
    // binds form submission and fields to the validation engine
    $(".clickOff").bind("contextmenu", function(e) {
        e.preventDefault();
    });
    
    $("a.linkNull").click(function() {
        return false;
    });
/* -------------------------------------------------------------------------- */
/* slich */  

$('.view-slick4').slick({
    infinite: true,
    adaptiveHeight: true,
    speed: 300,
    //autoplay: true,

    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 510,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});


/* -------------------------------------------------------------------------- */
/* fancybox */  
    $(".view-iframe").fancybox({
        openEffect: 'fade',
        titleShow: true,
        openMethod:'changeIn',
        padding: 2,
        helpers: {
            overlay :  {
                locked : false // item n�o vai ao TOP quando clicado
            },
            title: {
                type: 'float',
                title   : true
            },
            thumbs: {
                width: 50,
                height: 50,
                position: 'top'
            }
        }
    });
    
    $(".fancyboxIframe").fancybox({
            //maxWidth	: 1260,
            //maxHeight	: 800,
            fitToView	: false,
            width       : '95%',
            height      : '95%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'fade',
            closeEffect	: 'fade',
        iframe: {
            //scrolling : 'auto',
            preload   : true,
            //locked : false // item n�o vai ao TOP quando clicado
        },
        helpers: {
            overlay :  {
                locked : false // item n�o vai ao TOP quando clicado
            }
        }
    });
    
    function close_accordin_section() {
        $('a.acd-sct-title, a.acd-sct-icon').removeClass('active');
        $('.acd-sct-content').slideUp(300).removeClass('open');
    }
    $('a.acd-sct-title, a.acd-sct-icon').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).attr('href');
        if($(e.target).is('.active')) {
            close_accordin_section();
        }else {
            close_accordin_section();
            // Add active class to sct title
            $(this).addClass('active');
            // Open up the hidden content panel
            $(".acd " + currentAttrValue).slideDown(300).addClass('open');
        }
        e.preventDefault();
    });
/* -------------------------------------------------------------------------- */
/* Loader */
    /*
    window.onload = function() {
        $("#preLoader").fadeOut('slow');
        setTimeout(function(){
            $("#preLoader").removeClass("loading");
        },9999);
    };
    */

/* -------------------------------------------------------------------------- */
});

   
function carregaFlash(caminho,largura,altura,id){
	document.write('<object style="z-index:-2" id="'+id+'" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'+largura+'" height="'+altura+'">');
	document.write('<param name="movie" value="'+caminho+'">');
	document.write('<param name="quality" value="best">');
	document.write('<param name="menu" value="false">');
	document.write('<param name="wmode" value="transparent">');
	document.write('<embed style="z-index:-2" id="'+id+'" src="'+caminho+'" quality="best" type="application/x-shockwave-flash" width="'+largura+'" height="'+altura+'" wmode="transparent"></embed>');
	document.write('</object>');
}
