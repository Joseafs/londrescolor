<?php 
    if ($_POST['btnEnviar']=="Enviar") {

        $erro = "";

        if ($nome->isEmpty()) {
            $erro = "Preencha seu nome";
        } elseif ($email->isEmpty() || !$email->isMail()) {
            $erro = "Informe seu e-mail";
        } elseif ($telefone->isEmpty()) {
            $erro = "Informe o telefone";
        } elseif ($nomeproduto->isEmpty()) {
            $erro = "Por favor, recarregue a p�gina!";
        } elseif ($quantidade->isEmpty()) {
            $erro = "Informe a quantidade";
        } 

        if (empty($erro)) {
            $emailConteudo = $conn->query("SELECT * FROM CONTEUDOEMAIL WHERE ID_CONTEUDOEMAIL=2");
            $emailConteudo[0]['CONTEUDO'] = str_replace("%NOMEPRODUTO%", $_POST['nomeproduto'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%NOME%", $_POST['nome'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%EMAIL%", $_POST['email'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%TELEFONE%", $_POST['telefone'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%QUANTIDADE%", $_POST['quantidade'], $emailConteudo[0]['CONTEUDO']);

            $enviaremail = new Mail("LONDRESCOLOR - OR�AMENTO PRODUTO ESPEC�FICO", $config->getConfig(7), "LONDRESCOLOR OR�AMENTO", $emailConteudo[0]['CONTEUDO']);
            $enviaremail->setReplyTo($_POST['nome'], $_POST['email']);
            $enviaremail->setTo("LONDRESCOLOR - OR�AMENTO PRODUTO ESPEC�FICO", $config->getConfig(7));
            if ($enviaremail->enviar()) {
                            echo "<script>alert('Seu or�amento foi enviado com sucesso.\\n\\nEntraremos em contato o mais breve possivel.\\n\\nObrigado!')</script>";
            }else{
                    echo "<script>alert('Erro no envio do contato.\\n\\nTente novamente!')</script>";
            }
        } else {
            echo "<script>alert('".$erro."')</script>";
        }
    }
?>
<script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '212306842484368',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
</script>

<div class="container">
    <div class="content pdg30B sm-tCenter">
        <?php
            $idSecao = $_GET['id'];
            if(is_numeric($idSecao) && $idSecao > 0){
                $sql = "SELECT * FROM MATERIAL WHERE ID_MATERIAL=".$idSecao;
                $dadosSecao = $conn->query($sql);
                
                if(!empty($dadosSecao)){

                    echo '<h1 class="title cPrimary fDosis">'.$dadosSecao[0]['NM_MATERIAL'].'</h1>';
                    
                    $sqlBread = "SELECT DISTINCT MC.DS_MATERIALCATEGORIA ".
                                                                        "FROM MATERIALCATEGORIA MC, MATERIAL M ".
                                                                        "WHERE M.ID_MATERIALCATEGORIA = MC.ID_MATERIALCATEGORIA ".
                                                                        "AND M.ID_MATERIAL=".$_GET['id'];
                    $dadosBread = $conn->query($sqlBread);
                    echo    '<div class="fSize16 w100 floatL pdg3"> <span class="cPrimary">&raquo;</span>'.
                                '<a href="/produtos"> Produtos </a> <span class="cPrimary">&raquo;</span> '.
                                "<a href='/categoria/".$dadosSecao[0]['ID_MATERIALCATEGORIA']."/".Link::getStringBarra($dadosBread[0]['DS_MATERIALCATEGORIA'])."' title='".$dadosSecao[0]['DS_MATERIALCATEGORIA']."'".
                                    $dadosSecao[0]['DS_MATERIALCATEGORIA'].
                                "</a>".
                                
                                $dadosBread[0]['DS_MATERIALCATEGORIA'].
                            '</div>';
                    
                    if(file_exists("./arquivos/material/".$dadosSecao[0]['ID_MATERIAL'].".png")) {
                        $imgSecao = "<img class='floatL bRad3 sampleImg' src='/arquivos/material/".$dadosSecao[0]['ID_MATERIAL'].".png' alt='".$dadosSecao[0]['NM_MATERIAL']."' title='".$dadosSecao[0]['NM_MATERIAL']."' />";
                        $imgSecaoFace = "http://".$_SERVER['HTTP_HOST']."/arquivos/material/".$dadosSecao[0]['ID_MATERIAL'].".png";
                    }
        ?>
                    <div class="w100 floatL mgn20B tCenter">
                        <div class="socialIcones dInlineB tLeft sm-tCenter">
                            <div class="floatL sm-clearB sm-w100">
                                <a href="http://twitter.com/share" class="twitter-share-button" data-text="" data-count="horizontal">Tweet</a>
                                <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                                <script src="http://platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share" data-counter="right"></script>&nbsp;&nbsp;
                                <g:plusone size="medium"></g:plusone>
                            </div>
                            <div class="floatL sm-clearB sm-w100">
                                <button id="share_button" class="bRad3 effRipple floatL" alt="Compartilhar" title="Compartilhar" type="submit">Compartilhar</button>
                                <div class="socialIconesFacebook sm-floatR floatL mgn5L">
                                    <div class="fb-like" data-width="200" data-href="<?=$config->getConfig(7)?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                            $('#share_button').click(function(e){
                                                e.preventDefault();
                                                FB.ui(
                                                {
                                                    method: 'feed',
                                                    name: '<?=$dadosSecao[0]['NM_MATERIAL'] ?>',
                                                    link: '<?=$_SERVER['SCRIPT_URI']; ?>',
                                                    picture: '<?=$imgSecaoFace;?>',
                                                    caption: '<?=$dadosSecao[0]['NM_MATERIAL'] ?>',
                                                    description: '',
                                                    message: ''
                                                });
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="default floatL w100 tCenter">
                        <?php echo $imgSecao;?>

                        <div class="fSize16 tLeft">
                            <?=stripslashes($dadosSecao[0]['DS_MATERIAL']) ?>
                        </div>
                    </div>
                    <div class="w100 floatL pdg30T tCenter">
                        <h2 class="cPrimary fMed">Fa�a aqui o or�amento deste produto!</h2>
                        <form class="formInfo dInlineB w50 md-w80 sm-w100" method="POST" action="" >
                            <div class="inputField w100 pdg3">
                                <input id="iptNome" type="text" placeholder="Nome" name="nome" class="selectField bgOpac10-dark" required>
                            </div>
                            <div class="inputField w60 pdg3">
                                <input id="iptEmail" type="email" placeholder="E-mail" name="email" class="selectField bgOpac10-dark" required>
                            </div>
                            <div class="inputField w20 pdg3">
                                <input id="iptFone" type="tel" placeholder="Telefone" name="telefone" class="selectField bgOpac10-dark fone" required>
                            </div>
                            <div class="inputField w0 " hidden>
                                <input id="iptProduto" type="text" placeholder="<?=$dadosSecao[0]['NM_MATERIAL']?>" name="nomeproduto" class="selectField bgOpac10-dark cWhite" value="<?=$dadosSecao[0]['ID_MATERIAL']?> - <?=$dadosSecao[0]['NM_MATERIAL']?>" required hidden>
                            </div>
                            <div class="inputField w20 pdg3">
                                <input id="iptQtd" type="number" placeholder="quantidade" name="quantidade" class="selectField bgOpac10-dark" required max="999" min="1">
                            </div>
                            
                            <div class="inputField w100 pdg3 tCenter" >
                                <button name="btnEnviar" value="Enviar" class="bgPrimary effShadow effRipple fSize16 pdg8 pdg20R pdg20L bRad3 cWhite fMed" type="submit" >
                                    Enviar
                                </button>
                            </div>
                        </form>
                    </div>
        <?php 
                } else { header("Location: http://".$_SERVER['HTTP_HOST']);}
            } 
        ?>
        <a class="floatR bRad3 cPrimary fSize20 effRippleDark effShadow pdg8 ovflwH sm-w100" href="/produtos" alt="mais produtos" title="mais produtos" >+ Produtos</a>
    </div>
</div>